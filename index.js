// Crud operations
/*
	- CRUD operations are the heart of any backend applocation
	- Mastering the CRUD operation is essential for any developers
	- This helps in building character and increasing exposire to logical statements that will help us manipulate our data
	- Mastering the CRUD operation of any language makes us a valuable developer and make work easier for us to deal with huge ammounts of information
*/

// [SECTION] Inserting Documents (CREATE)

/*
	
*/

// Insert One - insert one document only
/*
	Creating a new documents we have a documents
	Syntax:
		- db.collectionName.insertOne({object});
*/

db.users.insertOne(
{
	firtName : "Jane",
	lastName : "Doe",
	age : 21,
	contact : {
		phone : "8796545",
		email : "janedoe@gmail.com"
	}
	courses : ["CSS", "JavaScript", "Python"],
	department : "none"
}
)

// Insert Many - inserts one or more documents
/*
	- Synatax
		- db.collectionName.insertMany([{ObjectA}, {ObjectB}]);
*/

db.users.insertMany(
	[
		{
			firtName : "Stephen",
			lastName : "Hawkins",
			age : 76,	
			contact : {
				phone : "90000",
				email : "stephenhawkins@gmil.com"
			},
			courses : ["Python", "React", "PHP"],
			department : "none"
		},
		{
			firtName : "Neil",
			lastName : "Armstrong",
			age : 82,	
			contact : {
				phone : "4777852",
				email : "neilarmstrong@gmil.com"
			},
			courses : ["React", "Laravel", "MongoDB"],
			department : "none"
		}
	]
)

//  error:  script executed successfully, there are no result to show = check spelling
// take note pag nagerror si robo 3t wag i run para hindi madublicate

// [SECTION] Upadate (UPDATE)

db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "00000000",
        email: "test@gmail.com"
    },
    courses: [],
    department: "none"
});
/*
	Syntax
		- db.collectionName.updateone({criteria}. {$set: {field/s : value/s}})
*/


db.users.updateOne(
	{ firstName: "Test" },
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "imrich@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
)


db.users.updateOne(

	{firtName : "Jane"},
	{
		$set : 
		{
			firtName : "Jenny"
		}
	}
)

// update many - updating multiple documents that matches field and value / criteria

db.users.updateMany(
	{department: "none"},
	{
		$set: 
		{
			department:"HR"
		}
	}
)

// [SECTION] Replace
/*
	- replaces all the fields in a documents/object
	db.users.replace({})


*/

db.users.replaceOne(
	{firstName : "Bill"}, // criteria - we will going to changa a specific field
	{
		firstName : "Elon",
		lastName : "Musk",
		age : 30,
		courses : ["PHP", "Laravel", "HTML"],
		status : "trippings"
	}
)

// [SECTION] Finding Documents (READ)
/*
	-db.collectionName.find();
	-db.collectionName.find({field: value});
*/

// retrieve all documents
db.users.find();
// retrieve specific documents/s
db.users.find({firtname: "Stephen"})
// multiple criteria
db.users.find({lastName: "Armstrong", age:82})

// [SECTION] Delete (DELETE)
/*
	db.collectionName.deleteOne({field: value});
	db.collectionName.deleteMany({field: value})
*/
// delete one documents that matches the field and value
db.users.deleteOne({
	firtName : "Jane"
})
db.users.deleteMany({
	firstName : "stephen"
})

db.users.find()


db.users.deleteMany({
	department : "HR"
})

// db.hotel.insertOne({object})
// insertOne - 1 (name: single)
// insertMany - 2 (name: double, name: queen)